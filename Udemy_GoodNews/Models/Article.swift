//
//  Article.swift
//  Udemy_GoodNews
//
//  Created by Jeffry Sandy Purnomo on 02/09/21.
//

import Foundation

struct ArticleList: Decodable{
    let articles: [Article]
}

struct Article: Decodable{
    let title: String
    let description: String?
}
