//
//  ArticleTableViewCell.swift
//  Udemy_GoodNews
//
//  Created by Jeffry Sandy Purnomo on 02/09/21.
//

import Foundation
import UIKit

class ArticleTableViewCell: UITableViewCell{
    
    static let identifier = "ArticleTableViewCell"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
}
